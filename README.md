For correct operation, you may need to install an chrome extension: "Cross Domain - CORS". (CORS error)

###### First option

1. [project link](https://a.dmitrieva94.gitlab.io/findsongs/)
2. Mixed Content error actions:
  * [step 1 screenshot](http://prntscr.com/10iylnu)
  * [step 2 screenshot](http://prntscr.com/10iymr2)
  * [step 3 screenshot](http://prntscr.com/10iyp7d)

[if the links above don't work](https://docs.google.com/document/d/1y7FlS3b0BNogS_SE7QRQakWVR9fo4HEk59VrRhp4FsE/edit?usp=sharing)

###### Second option

1. Сlone this repository
2. npm install
3. npm run start
