export const API_BASE_URL = 'http://api.chartlyrics.com/apiv1.asmx/';

export const getSong = (url) => {
  let tryesLeft = 3;
  const doReqest = () => fetch(API_BASE_URL + url);
  return new Promise((resolve, reject) => {
    const onDone = (response) => {
      tryesLeft--;
      if(response.ok){
        const result = response.text().then(str => (new window.DOMParser()).parseFromString(str, "text/xml"));
        resolve(result);
      } else if (tryesLeft > 0) {
        doReqest().then(onDone).catch(onError);
      } else {
        resolve();
      }
    };
    const onError = () => {
      reject();
    };
    doReqest().then(onDone).catch(onError);
  });
};

export const getShortSongName = songName => {
  let nameArr = songName.split(' ');
  if (nameArr.length > 2) {
    nameArr = nameArr.splice(0, 2);
  }
  return nameArr.join(' ');
};
