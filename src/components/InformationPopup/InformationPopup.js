import React, { useEffect, useState } from 'react';
import logo from '../../images/note.png';
import { getSong } from '../../apiMetods/metods';
import './InformationPopup.css'

const InformationPopup = ({item, onFavoriteClick, onClose}) => {
  const [ data, setData ] = useState({
    artist: '',
    song: '',
    lyricCheckSum: '',
    id: '',
    favorite: false,
    imgUrl: '',
    text: '',
  });

  useEffect(()=>{
    const informationPopup = new window.bootstrap.Modal(document.getElementById('informationPopup'));
    informationPopup.show();
    const informationPopupElem = document.getElementById('informationPopup');

    informationPopupElem.addEventListener('hidden.bs.modal', () => {
      setData({
        artist: '',
        song: '',
        lyricCheckSum: '',
        id: '',
        imgUrl: '',
        text: '',
      });
      onClose();
    });

    informationPopupElem.addEventListener('shown.bs.modal',
      getSong(`GetLyric?lyricId=${item.id}&lyricCheckSum=${item.lyricCheckSum}`)
        .then((xml) => {
          if (xml) {
            const imgUrl = xml.querySelector('LyricCovertArtUrl');
            const artist = xml.querySelector('LyricArtist').innerHTML;
            const song = xml.querySelector('LyricSong').innerHTML;
            setData({
              artist: artist,
              song: song,
              id: item.id,
              lyricCheckSum: item.lyricCheckSum,
              imgUrl: imgUrl ? imgUrl.innerHTML : undefined,
              text: xml.querySelector('Lyric').innerHTML,
            })
          } else {
            setData({
              artist: '',
              song: '',
              id: '',
              lyricCheckSum: '',
              imgUrl: '',
              text: 'Oups... Something wrong!',
            });
          }
        }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const onIconCLick = () => {
    onFavoriteClick(data, true);
  };

  return <div className="modal fade" id='informationPopup' tabIndex="-1" aria-hidden="true">
    <div className="modal-dialog modal-lg">
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">{data.artist} - {data.song}</h4>
          {!!data.id && <div className='after' onClick={onIconCLick}>{item.favorite ? '★' : '☆'}</div>}
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"/>
        </div>
        <div className="modal-body">
          <div className="songText">
          <div className='photoProfile'>
            <img src={data.imgUrl ? data.imgUrl : logo} alt='Profile'/>
          </div>
            {data.text}
          </div>
        </div>
      </div>
    </div>
  </div>
};

export default InformationPopup;
