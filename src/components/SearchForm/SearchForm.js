import React, { useState } from 'react';
import './SearchForm.css'

const SearchForm = ({type, onSubmit}) => {
  const [valueFirstInput, setValueFirstInput] = useState('');
  const [valueSecondInput, setValueSecondInput] = useState('');
  const [valueThirdInput, setValueThirdInput] = useState('');

  const onChangeValue = (e) => {
    e.target.name === 'artist' && setValueFirstInput(e.target.value);
    e.target.name === 'song' && setValueSecondInput(e.target.value);
    e.target.name === 'text' && setValueThirdInput(e.target.value);
  };

  return <form className='searchArtist' onSubmit={onSubmit} >
    {type === 'artist'
      ? <>
        <div className='searchTitle'>Artist</div>
        <input
          name="artist"
          type="text"
          value={valueFirstInput}
          onChange={onChangeValue}
          className="form-control search-input"
          placeholder="Enter artist name"
          required
        />
        <div className='searchTitle'>Song</div>
        <input
          name="song"
          type="text"
          value={valueSecondInput}
          onChange={onChangeValue}
          className="form-control search-input"
          placeholder="Enter the title of the song"
          required
        />
      </>
      : <>
        <div className='searchTitle'>Text</div>
        <input
          name="text"
          type="text"
          value={valueThirdInput}
          onChange={onChangeValue}
          className="form-control search-input"
          placeholder="Enter a line from the song"
          required
        />
      </>
    }
    <button type='submit' className='btn btn btn-primary'>Search ♫</button>
  </form>;
};

export default SearchForm;
