import React from 'react';
import './TabsPanel.css'

const TabsPanel = ({onChangeView, searchType}) => {
  return (
    <div className='btn-group'>
      <button type='button'
          onClick={() => onChangeView('artist')}
          className={`btn ${searchType === 'artist' ? 'btn-primary' : 'btn-outline-secondary'}`}>
          Search for artist name
      </button>
      <button type='button'
          onClick={() => onChangeView('text')}
          className={`btn ${searchType === 'text' ? 'btn-primary' : 'btn-outline-secondary'}`}>
          Search for song text
      </button>
      <button type='button'
          onClick={() => onChangeView('favorites')}
          className={`btn ${searchType === 'favorites' ? 'btn-primary' : 'btn-outline-secondary'}`}>
          Favorites
      </button>
    </div>
  );
};

export default TabsPanel;
