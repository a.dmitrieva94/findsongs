import React from 'react';
import './ResultCard.css'

const ResultCard = ({item, onRowClick, onFavoriteClick}) => {

  const onIconCLick = event => {
    event.preventDefault();
    event.stopPropagation();
    onFavoriteClick(item);
  };
  
  const onCLickRow = () => {
    onRowClick(item);
  };

  return <div className={`col`}
    onClick={onCLickRow}
  >
    <div className='colContent'>
      <p><strong>{item.artist}</strong></p>
      <p><strong>{item.song}</strong></p>
    </div>
    <div className='after' onClick={onIconCLick}>{item.favorite ? '★' : '☆'}</div>
  </div>
};

export default ResultCard;
