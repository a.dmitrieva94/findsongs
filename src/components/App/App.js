import React, { Component } from 'react';
import SearchForm from '../SearchForm/SearchForm';
import TabsPanel from '../TabsPanel/TabsPanel';
import ResultCard from '../ResultCard/ResultCard';
import InformationPopup from '../InformationPopup/InformationPopup';
import { getSong, getShortSongName } from '../../apiMetods/metods';
import './App.css';

export default class App extends Component {
  state = {
    searchType: 'artist',
    searchResultItems: [],
    hasError: null,
    informationPopup: null,
  };
  favorites = JSON.parse(localStorage.getItem('favorites')) || {};
  onChangeView = filterValue => {
    const newSearchResultItems = filterValue === 'favorites'
      ? this.getFavoritesItems()
      : [];

    this.setState({
      searchType: filterValue,
      searchResultItems: newSearchResultItems,
      hasError: null,
    });
  };
  onSubmit = event => {
    event.preventDefault();
    const formData = new FormData(event.target);

    const requestUrl = this.state.searchType === 'artist'
      ? `SearchLyric?artist=${formData.get('artist')}&song=${getShortSongName(formData.get('song'))}`
      : `SearchLyricText?lyricText=${formData.get('text')}`;

    getSong(requestUrl).then((res) => {
      if (res) {
        const resultArray = res.querySelectorAll('SearchLyricResult');
        const result = [];
        resultArray.forEach(el => {
          const artist = el.querySelector('Artist');
          const song = el.querySelector('Song');
          const id = el.querySelector('LyricId');
          const lyricCheckSum = el.querySelector('LyricChecksum');
          if (id && lyricCheckSum) {
            result.push({
              artist: artist.innerHTML,
              song: song.innerHTML,
              id: id.innerHTML,
              lyricCheckSum: lyricCheckSum.innerHTML,
              text: '',
              favorite: this.favorites[id.innerHTML] ? true : false,
            });
          }
        });
        if (!!result.length) {
          this.setState({searchResultItems: result, hasError: null});
        } else {
          this.setState({searchResultItems: [], hasError: {type: 'search empty', id: 2}});
        };
      } else {
        this.setState({searchResultItems: [], hasError: {type: 'request failed', id: 1}});
      }
    })
  };

  onRowClick = data => {
    this.setState({informationPopup: {
      id: data.id,
      lyricCheckSum: data.lyricCheckSum,
      favorite: this.favorites[data.id] ? true : false,
    }});
  };

  onClosePopup = () => {
    this.setState({informationPopup: null});
  };

  toogleSongToFavorite = (data, popup = false) => {
    const { searchResultItems } = this.state;
    const index = searchResultItems.findIndex((el) => el.id === data.id);

    if (searchResultItems[index].favorite) {
      searchResultItems[index].favorite = false;
      delete this.favorites[data.id];
      if ( this.state.searchType === 'favorites') {
        searchResultItems.splice(index, 1);
      }
    } else {
      data.favorite = true;
      if (!this.favorites[data.id]) {
        this.favorites[data.id] = {
          artist: data.artist,
          song: data.song,
          id: data.id,
          lyricCheckSum: data.lyricCheckSum,
        };
      }
      searchResultItems[index] = data;
    }

    localStorage.setItem('favorites', JSON.stringify(this.favorites));
    this.setState({searchResultItems});

    if (popup) {
      this.setState({informationPopup: {...this.state.informationPopup, favorite: data.favorite}});
    }
  };

  getFavoritesItems = () => {
    return Object.keys(this.favorites).map(id => {
      this.favorites[id].favorite = true;
      return this.favorites[id];
    });
  };

  render(){
    const {searchType, searchResultItems, informationPopup, hasError} = this.state;
    const errorTexts = ['Oups... Somesing wrong!', 'No results were found for your search'];

    return (
      <div className="search-app">
        <h1>Find your favorite music easily</h1>
        <TabsPanel
          searchType={searchType}
          onChangeView={this.onChangeView}
        />
        {searchType !== 'favorites'
          && <SearchForm type={searchType} onSubmit={this.onSubmit}/>
        }
        {!!searchResultItems.length
          && <div className="container">
            <div className="row">
              {searchResultItems.map(el =>
                <ResultCard
                  item={el}
                  key={el.id}
                  onRowClick={this.onRowClick}
                  onFavoriteClick={this.toogleSongToFavorite}
                />
              )}
            </div>
          </div>
        }
        {!searchResultItems.length && searchType === 'favorites'
          && <div style={{margin: `10px 0`}}>There is nothing in the favorites</div>
        }
        {!!informationPopup
          && <InformationPopup
            item={informationPopup}
            onClose={this.onClosePopup}
            onFavoriteClick={this.toogleSongToFavorite}
          />
        }
        {!!hasError && <div style={{margin: `10px 0`}}>{errorTexts[hasError.id-1]}</div>}
      </div>
    );
  };
};
